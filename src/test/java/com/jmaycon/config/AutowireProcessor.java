package com.jmaycon.config;

import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.api.extension.TestInstancePostProcessor;
import org.springframework.beans.factory.annotation.AutowiredAnnotationBeanPostProcessor;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class AutowireProcessor implements TestInstancePostProcessor {

    private static final AnnotationConfigApplicationContext APPLICATION_CONTEXT;
    private static final AutowiredAnnotationBeanPostProcessor AUTOWIRED_ANNOTATION_BEAN_POST_PROCESSOR;


    public static int instantiation = 1;
    public static int createInstance = 1;

    static {
        APPLICATION_CONTEXT = new AnnotationConfigApplicationContext(AutowireConfiguration.class);
        AUTOWIRED_ANNOTATION_BEAN_POST_PROCESSOR = new AutowiredAnnotationBeanPostProcessor();
        AUTOWIRED_ANNOTATION_BEAN_POST_PROCESSOR.setBeanFactory(APPLICATION_CONTEXT.getBeanFactory());
    }

    @Override
    public void postProcessTestInstance(Object testInstance, ExtensionContext context) {
        AUTOWIRED_ANNOTATION_BEAN_POST_PROCESSOR.processInjection(testInstance);
        createInstance++;

    }
}
