package com.jmaycon.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.jmaycon")
public class AutowireConfiguration {

}
