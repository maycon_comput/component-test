package com.jmaycon.client;

import com.jmaycon.config.RunWithDependencyInjectionSupport;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.jupiter.api.Assertions.assertEquals;

@RunWithDependencyInjectionSupport
public class TestClient {

    private Integer test = 1;

    @Autowired
    private ClientRest clientRest;


    @Test
    public void test() {
        assertEquals("Alexia", clientRest.getClientNameById(1));
    }

    @Test
    public void testNoStateShared1() {
        assertEquals(Integer.valueOf(1), test);
        test++;
    }

    @Test
    public void testNoStateShared2() {
        assertEquals(Integer.valueOf(1), test);
        test++;
    }

    @Test
    public void testNoStateShared3() {
        assertEquals(Integer.valueOf(1), test);
        test++;
    }
}
