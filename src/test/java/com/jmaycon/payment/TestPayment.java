package com.jmaycon.payment;

import com.jmaycon.config.RunWithDependencyInjectionSupport;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.jupiter.api.Assertions.assertTrue;

@RunWithDependencyInjectionSupport
public class TestPayment {

    @Autowired
    private PaymentRest paymentRest;

    @Test
    public void test() {
        assertTrue(paymentRest.performPayment());
    }
}
