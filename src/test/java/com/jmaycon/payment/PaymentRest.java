package com.jmaycon.payment;

import com.jmaycon.system.Request;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class PaymentRest {

    @Autowired
    private Request request;

    public boolean performPayment(){
        request.httpGet("/payment");
        return true;
    }

}
