package com.jmaycon.system;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class RequestImpl implements Request {

    @Override
    public void httpGet(String path) {
        log.info("Simulating a get to {} ", path);
    }
}
